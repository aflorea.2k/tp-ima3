Contrôle TP IMA3
================

Fichiers
--------

Dans le dépôt se trouvent lse fichiers suivants:
* `main.c` contenant le main
* `trees.h` contenant les en-têtes de toutes les fonctions utilsées
* `trees.c` contenant les fonctions utilisées.
* `TP_andrei_florea`.
* `Makefile` permettant la compilation.
* `samples/` contenant tous les fichiers permettant les tests.

Compilation
-----------

Pour compiler, il suffit d'éxecuter `make`; Ceci crée le programme `main`.

Avec `make warn`, on peut voir les éventuels warnings de `-Wall -Wextra`.

Avec `make debug`, on compile avec `-g -O0` (et les mêmes arguments que `warn`).