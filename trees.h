/*
 * FLOREA Andrei
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int val;
	struct node *left;
	struct node *right;
}Node, *PtNode, *Tree;
/* These typedefs are optional, you may use them */


/* Constructs a new tree from a value for the root node, a given left tree and a given right tree */
void cons_tree(struct node **, int, struct node *, struct node *);

/* Make an empty tree */
void mk_empty_tree(struct node **);

/* Is the tree empty ? */
bool is_empty(struct node *);

/* Is the tree a leaf ? */
bool is_leaf(struct node *);

/* Adds the value (int) to the binary search tree,
 * it must be ordered.
 * Duplicate values are valid.
 */
void add(struct node **, int);

/* Prints the values of the tree in ascendant order */
void print_tree(struct node *);

/* Builds a tree adding values of the file */
void load_tree(FILE *, struct node **);

/* Frees all the tree's memory */
void free_tree(struct node **);

/* gets max value in tree */
int get_max(struct node *);

/* counts the number of nodes in tree */
int count_nodes(struct node *);

/* searches value in tree */
int tree_search(struct node *, int);

/* sums values in tree */
int sum_tree(struct node*);

/* averages values in tree */
int * average_tree(struct node*);

/* prints values above a threshold */
void print_sup_nodes(struct node*, int);

struct cell{
  int val;
  struct cell* next;
};

/* converts tree to a sorted linked list */
void tree_to_listechainee(struct node*, struct cell**);

/* prints linked list */
void print_listechainee(struct cell* array);

/* frees linked list */
void free_listechainee(struct cell ** ptr_l);
