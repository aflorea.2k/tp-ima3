$(CC) = gcc

debug: ARGS = -g -O0
debug: warn

warn: ARGS += -Wall -Wextra -pedantic
warn: main

default: main

main: main.o trees.o
	$(CC) -o $@ $^ $(ARGS)

main.o: main.c trees.h
	$(CC) -c -o $@ $< $(ARGS)

trees.o: trees.c trees.h
	$(CC) -c -o $@ $< $(ARGS)


clean:
	rm main *.o
