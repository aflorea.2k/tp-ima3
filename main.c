/*
 * FLOREA Andrei
 */

#include <stdio.h>
#include "trees.h"


int main (int argc, char *argv[])
{
  // Manage arguments
  if(argc != 2){
    printf("Usage : %s <file>\n", argv[0]);
    return EXIT_SUCCESS;
  }

  // Main body:
  FILE* f = fopen(argv[1], "r");
  if(!f){
    printf("File %s could not be opened. Exiting.\n", argv[1]);
    return EXIT_FAILURE;
  }
  
  struct node * A;
  mk_empty_tree(&A);
  load_tree(f, &A);
  fclose(f);
  print_tree(A);
  printf("\n");
  printf("Max value:\n  %d\n", get_max(A));
  printf("Node count:\n  %d\n", count_nodes(A));

  // Using average_tree :
  int * av = average_tree(A);
  if(av)
    printf("Average:\n  %d\n", *av);
  else
    printf("Tree is empty, could not compute average.");

  // Using tree_search :
  int x;
  printf("Search number in tree: ");
  scanf("%d", &x);
  printf("Is %d in tree:\n", x);
  if(tree_search(A, x)){
    printf("  %d was found in tree.", x);
  }
  else
    printf("  %d was not found in tree.", x);
  printf("\n");
  
  // Using print_sup_nodes :
  printf("Print all values above: ");
  scanf("%d", &x);
  print_sup_nodes(A, x);
  printf("\n");

  //Using tree_to_listechainee
  printf("Converting to sorted linked list:\n");
  struct cell* l = NULL;
  tree_to_listechainee(A, &l);
  print_listechainee(l);
  
  printf("\nExiting..\n");
  // Exit program
  free(av);
  free_tree(&A);
  free_listechainee(&l);
  return EXIT_SUCCESS;
}

