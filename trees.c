/*
 * FLOREA Andrei
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node ** ptr_tree, int val, struct node *left, struct node *right)
{
  *ptr_tree = malloc(sizeof(struct node*));
  (*ptr_tree)->val=val;
  (*ptr_tree)->left=left;
  (*ptr_tree)->right=right;
}


// initialize un empty tree
void mk_empty_tree(struct node ** ptr_tree)
{
  *ptr_tree = NULL;
}

// is tree empty?
bool is_empty(struct node *tree)
{
  return tree==NULL;
}

// is tree a leaf?
bool is_leaf(struct node *tree)
{
  return(!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x)
{
  if(is_empty(*ptr_tree)){
    cons_tree(ptr_tree, x, NULL, NULL);
  }
  else{
    if(x < (*ptr_tree)->val){
      add(&(*ptr_tree)->left, x);
    }
    else if(x > (*ptr_tree)->val){
      add(&(*ptr_tree)->right, x);
    }
  }
}

// print values of tree in ascendant order
void print_tree(struct node *tree)
{
  if(!is_empty(tree)){
    print_tree(tree->left);
    printf("%d ", tree->val);
    print_tree(tree->right);
  }
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree)
{
  int px;
  while(fscanf(fp, "%d", &px) != EOF){
    add(ptr_tree, px);
  }
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree)
{
  if(!is_empty(*ptr_tree)){
    free_tree(&(*ptr_tree)->left);
    free_tree(&(*ptr_tree)->right);
    free(*ptr_tree);
  }
}

// Get biggest value in tree
int get_max(struct node * tree){
  if(!is_empty(tree->right))
    return get_max(tree->right);
  else
    return tree->val;
}

// Get number of nodes in tree
int count_nodes(struct node * tree){
  if(is_empty(tree))
    return 0;
  else
    return 1 + count_nodes(tree->left) + count_nodes(tree->right);
}

// Search value in tree
// Returns 1 if found, else 0
int tree_search(struct node * tree, int x){
  if (is_empty(tree))
    return 0;
  else if(x == tree->val)
    return 1;
  else if(x < tree->val)
    return tree_search(tree->left, x);
  else
    return tree_search(tree->right, x);
}

// Returns sum of all nodes in tree
int sum_tree(struct node* tree){
  if(is_empty(tree))
    return 0;
  else
    return tree->val + sum_tree(tree->left) + sum_tree(tree->right);
}

// Returns pointer to average of all nodes in tree
// If tree is empty, returns a NULL pointer
int * average_tree(struct node* tree){
  int c = count_nodes(tree);
  if(!c)
    return NULL;
  else{
    int *r = malloc(sizeof(int));
    *r = sum_tree(tree)/(float)c;
    return r;
  }
}

// Prints all values in 'tree' above 'threshold'
void print_sup_nodes(struct node* tree, int threshold){
  if(!is_empty(tree)){
    if(tree->val > threshold){
      print_sup_nodes(tree->left, threshold);
      printf("%d ", tree->val);
    }
    print_sup_nodes(tree->right, threshold);
  }
}

// Stores all values of 'tree' into 'array', a linked list.
void tree_to_listechainee(struct node* tree, struct cell** l){
  if(!is_empty(tree)){
    tree_to_listechainee(tree->right, l);
    
    struct cell* c= malloc(sizeof(struct cell));
    c->val = tree->val;
    c->next = *l;
    *l = c;

    tree_to_listechainee(tree->left, l);
  }
}

void print_listechainee(struct cell* l){
  if(l){
    printf("%d ", l->val);
    print_listechainee(l->next);
  }
}

void free_listechainee(struct cell ** ptr_l){
  if(*ptr_l){
    free_listechainee(&(*ptr_l)->next);
    free(*ptr_l);
  }
}
